
## Kata 5


## Rearrange the Sentence 
---
Published by BijogFc24 in JavaScript 
Algorithms, logic, sorting, strings 
---

Given a sentence with numbers representing a word's location 
embedded within each word, return the sorted sentence. 
# Examples 

---

rearrange("is2 Thi1s T4est 3a") ➞ "This is a Test" 
 
rearrange("4of Fo1r pe6ople g3ood th5e the2") ➞ "For the good of the 
people" 
 
rearrange("5weird i2s JavaScri1pt dam4n so3") ➞ "JavaScript is so 
damn weird" 
 
rearrange(" ") ➞ "" 
# Notes 
• Only the integers 1-9 will be used 
• You may assume no special characters will be used 
 
 
 