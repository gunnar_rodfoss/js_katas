 export function rearrange(sentence){
        if(sentence === " "){
            return "";
        }
        let wordArr = sentence.split(" ")
        let newWord = [];
        let numbersArr = [];

        for(let i = 0; i < wordArr.length; i++){

            let numbers = wordArr[i].match( /\d/g, '');
            numbersArr.push(numbers)
            //console.log(numbers)
        }
        for(let i = 0; i < wordArr.length-1; i++){
            for(let j = 0; j < wordArr.length-1; j++){
                if( parseInt(numbersArr[j]) > parseInt(numbersArr[j+1]) ){
                    let temp = numbersArr[j]
                    numbersArr[j] = numbersArr[j+1]
                    numbersArr[j+1] = temp
                    let tempWord =  wordArr[j].replace( /\d/g, '');
                    wordArr[j] =  wordArr[j+1].replace( /\d/g, '');
                    wordArr[j+1] = tempWord
                }
            }
        }
       // console.log(numbersArr)
        return wordArr.join(' ')
       // return wordArr.sort();
    
}

//console.log(rearrange("is2 Thi1s T4est 3a"));
//console.log(rearrange("4of Fo1r pe6ople g3ood th5e the2"));
/*
console.log(elasticWord("ANNA") );
console.log(elasticWord("KAYAK") );
*/