import {rearrange} from	'./index';

it("shuld be This is a Test for is2 Thi1s T4est 3a", function(){
    const word = "is2 Thi1s T4est 3a";
    const newWord = "This is a Test";

    expect(rearrange(word)).toEqual(newWord);
})
it("shuld be For the good of the people for 4of Fo1r pe6ople g3ood th5e the2", function(){
    const word = "4of Fo1r pe6ople g3ood th5e the2";
    const newWord = "For the good of the people";

    expect(rearrange(word)).toEqual(newWord);
})
it("shuld be JavaScript is so damn weird for 5weird i2s JavaScri1pt dam4n so3", function(){
    const word = "5weird i2s JavaScri1pt dam4n so3";
    const newWord = "JavaScript is so damn weird";

    expect(rearrange(word)).toEqual(newWord);
})
it("shuld be for  ", function(){
    const word = " ";
    const newWord = "";

    expect(rearrange(word)).toEqual(newWord);
})

/*// Comparing Strings
test('hello world should equal hello world', () => {
	expect("hello world").toBe("hello world");
});

// Compare numbers
test('10 + 5 should equal 15', () => {
	expect(10 + 5).toBe(15);
})

// Compare numbers to not equal
test('10 + 4 should not equal 15', () => {
	expect(10 + 4).not.toBe(15);
})

// Compare arrays
test("[1, 2, 3] should equal [1,2,3]", () => {
	expect([1, 2, 3]).toEqual([1, 2, 3])
})

// Compare arrays to not equal
test("[1, 2, 3] should not equal [1,2,3,4]", () => {
	expect([1, 2, 3]).not.toEqual([1, 2, 3,4])
})

// Compare objects
test("{ message: 'hello' } should equal { message: 'hello' }", () => {
	expect([1, 2, 3]).toEqual([1, 2, 3])
})

// Compare objects to not equal
test("{ message: 'hello' } should not equal { message: 'goodbye' }", () => {
	expect({ message: 'hello' }).not.toEqual({ message: 'goodbye' })
})


*/