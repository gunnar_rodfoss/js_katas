import {wordSplitter} from	'./index';

it("shuld be [‘let’, ‘ter’] for Letter", function(){
    const word = "Letter";
    const actArr = ["let", "ter"];

    expect(wordSplitter(word)).toEqual(actArr);
})
it("shuld be [‘real’, ‘ly’] for Really", function(){
    const word = "Really";
    const actArr = ["real", "ly"];

    expect(wordSplitter(word)).toEqual(actArr);
})
it("shuld be [‘hap’, ‘py’] for Happy", function(){
    const word = "Happy";
    const actArr = ["hap", "py"];

    expect(wordSplitter(word)).toEqual(actArr);
})
it("shuld be [‘shal’, ‘l’] for Shall", function(){
    const word = "Shall";
    const actArr = ["shal", "l"];

    expect(wordSplitter(word)).toEqual(actArr);
})
it("shuld be [‘to’, ‘ol’] for Tool", function(){
    const word = "Tool";
    const actArr = ["to", "ol"];

    expect(wordSplitter(word)).toEqual(actArr);
})
it("shuld be [] for Easy", function(){
    const word = "Easy";
    const actArr = [];

    expect(wordSplitter(word)).toEqual(actArr);
})
it("shuld be[ 'mis', 'sis', 'sip', 'pi' ] for Mississippi", function(){
    const word = "Mississippi";
    const actArr = [ 'mis', 'sis', 'sip', 'pi' ];

    expect(wordSplitter(word)).toEqual(actArr);
})
/*// Comparing Strings
test('hello world should equal hello world', () => {
	expect("hello world").toBe("hello world");
});

// Compare numbers
test('10 + 5 should equal 15', () => {
	expect(10 + 5).toBe(15);
})

// Compare numbers to not equal
test('10 + 4 should not equal 15', () => {
	expect(10 + 4).not.toBe(15);
})

// Compare arrays
test("[1, 2, 3] should equal [1,2,3]", () => {
	expect([1, 2, 3]).toEqual([1, 2, 3])
})

// Compare arrays to not equal
test("[1, 2, 3] should not equal [1,2,3,4]", () => {
	expect([1, 2, 3]).not.toEqual([1, 2, 3,4])
})

// Compare objects
test("{ message: 'hello' } should equal { message: 'hello' }", () => {
	expect([1, 2, 3]).toEqual([1, 2, 3])
})

// Compare objects to not equal
test("{ message: 'hello' } should not equal { message: 'goodbye' }", () => {
	expect({ message: 'hello' }).not.toEqual({ message: 'goodbye' })
})


*/