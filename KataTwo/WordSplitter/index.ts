 export function wordSplitter(word){
     word = word.toLowerCase();
    let wordArr = [];
    for(let i = 0; i < word.length-1; i++){

        if(word[i] === word[i+1]){
            wordArr.push( word.slice(0,i+1) )
            word = word.slice(i+1,word.length);
            i=0;
        }

    }
    wordArr.push( word )
    if(wordArr.length < 2){
        return [];
    }
    else{
        return wordArr;
    }
    
}

//console.log(wordSplitter("Mississippi") );
//console.log(wordSplitter("Really") );
//console.log(wordSplitter("happy") );