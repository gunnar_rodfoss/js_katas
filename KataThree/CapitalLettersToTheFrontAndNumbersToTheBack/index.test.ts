import {reorder} from	'./index';

it("shuld be APhpy24 for hA2p4Py", function(){
    const word = "hA2p4Py";
    const newWord = "APhpy24";

    expect(reorder(word)).toEqual(newWord);
})
it("shuld be MENTmove11 for m11oveMENT", function(){
    const word = "m11oveMENT";
    const newWord = "MENTmove11";

    expect(reorder(word)).toEqual(newWord);
})
it("shuld be OCAKEshrt94 for s9hOrt4CAKE", function(){
    const word = "s9hOrt4CAKE";
    const newWord = "OCAKEshrt94";

    expect(reorder(word)).toEqual(newWord);
})

/*// Comparing Strings
test('hello world should equal hello world', () => {
	expect("hello world").toBe("hello world");
});

// Compare numbers
test('10 + 5 should equal 15', () => {
	expect(10 + 5).toBe(15);
})

// Compare numbers to not equal
test('10 + 4 should not equal 15', () => {
	expect(10 + 4).not.toBe(15);
})

// Compare arrays
test("[1, 2, 3] should equal [1,2,3]", () => {
	expect([1, 2, 3]).toEqual([1, 2, 3])
})

// Compare arrays to not equal
test("[1, 2, 3] should not equal [1,2,3,4]", () => {
	expect([1, 2, 3]).not.toEqual([1, 2, 3,4])
})

// Compare objects
test("{ message: 'hello' } should equal { message: 'hello' }", () => {
	expect([1, 2, 3]).toEqual([1, 2, 3])
})

// Compare objects to not equal
test("{ message: 'hello' } should not equal { message: 'goodbye' }", () => {
	expect({ message: 'hello' }).not.toEqual({ message: 'goodbye' })
})


*/