## Kata 3
---
Move Capital Letters to the Front and numbers to the 
back  
Original version Published by Helen Yu in  JavaScript 
Formatting, regex, strings 
Altered by Dewald Els 
Create a function that accepts a string as an argument.  
The function must move all capital letters to the front of a word, lowercase letters thereafter 
and lastly, numbers to the back. It should return a string with the reordered word. 
# Examples 

---

reorder("hA2p4Py") ➞ "APhpy24" 
 
reorder ("m11oveMENT") ➞ "MENTmove11" 
 
reorder ("s9hOrt4CAKE") ➞ "OCAKEshrt94" 
 
Notes Keep the original relative order of the upper and lower case letters the same as well as 
numbers 
 
 