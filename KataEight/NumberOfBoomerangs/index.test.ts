import {countBoomerangs} from	'./index';

it("shuld be 2 for [9, 5, 9, 5, 1, 1, 1]", function(){
    const Boomerangs = [9, 5, 9, 5, 1, 1, 1];
    const BoomerangsAmount = 2;

    expect(countBoomerangs(Boomerangs)).toEqual(BoomerangsAmount);
})
it("shuld be 1 for [5, 6, 6, 7, 6, 3, 9]", function(){
    const Boomerangs = [5, 6, 6, 7, 6, 3, 9];
    const BoomerangsAmount = 1;

    expect(countBoomerangs(Boomerangs)).toEqual(BoomerangsAmount);
})
it("shuld be 0 for [4, 4, 4, 9, 9, 9, 9]", function(){
    const Boomerangs = [4, 4, 4, 9, 9, 9, 9];
    const BoomerangsAmount = 0;

    expect(countBoomerangs(Boomerangs)).toEqual(BoomerangsAmount);
})

it("shuld be 5 for [1, 7, 1, 7, 1, 7, 1]", function(){
    const Boomerangs = [1, 7, 1, 7, 1, 7, 1];
    const BoomerangsAmount = 5;

    expect(countBoomerangs(Boomerangs)).toEqual(BoomerangsAmount);
})
