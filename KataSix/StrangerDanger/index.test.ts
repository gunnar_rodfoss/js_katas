import {noStrangers} from	'./index';

it("shuld be [[See,Spot],[]] for See Spot run. See Spot jump. Spot likes jumping. See Spot fly.", function(){
    const word = "is2 Thi1s T4est 3a";
    const newWord = [["See","Spot"],[]];

    expect(noStrangers(word)).toEqual(newWord);
})
