import {caesarCipher} from	'./index';

it("shuld be okffng-Qwvb for middle-Outz", function(){
    const word = "middle-Outz";
    const newWord = "okffng-Qwvb";

    expect(caesarCipher(word,2)).toEqual(newWord);
})
it("shuld be Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj for Always-Look-on-the-Bright-Side-of-Life", function(){
    const word = "Always-Look-on-the-Bright-Side-of-Life";
    const newWord = "Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj";

    expect(caesarCipher(word,5)).toEqual(newWord);
})
it("shuld be okffng-Qwvb for A friend in need is a friend indeed", function(){
    const word = "A friend in need is a friend indeed";
    const newWord = "U zlcyhx ch hyyx cm u zlcyhx chxyyx";

    expect(caesarCipher(word,20)).toEqual(newWord);
})

