

function shuffleCount(deckSize) {
    let deck = []
    let shuffleTimes = 0
    for (let i = 1; i <= deckSize; i++) {
        deck.push(i)
    }

   let response = deck
   do{
        response = shuffle(response)
        shuffleTimes++;
   }
   while(JSON.stringify(response) !== JSON.stringify(deck) )

    return shuffleTimes;
}

function shuffle(deck){
    let decSplitOne = []
    let decSplitTwo = []
    let newDeck = []
 
    for (let i = 0; i < deck.length - deck.length / 2; i++) {
        decSplitOne.push(deck[i])
    }
    for (let i = deck.length - deck.length / 2; i < deck.length; i++) {
 
        decSplitTwo.push(deck[i])
    }
   
    for (let i = 0; i < decSplitOne.length; i++) {
        newDeck.push(decSplitOne[i])
        newDeck.push(decSplitTwo[i])
    }
    return newDeck
}

console.table(shuffleCount(8));
console.table(shuffleCount(14));
console.table(shuffleCount(52));

