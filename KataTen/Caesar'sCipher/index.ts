
 export function caesarCipher(word, num){
    let newWordArr = []
    let newWord = ""
    for(let i = 0; i < word.length; i++){
        if(word[i] != ' ' && word[i] != '-'){
            newWordArr.push(word.charCodeAt(i) )
        }
        else{
            newWordArr.push(word[i]);
        }
    }
    for(let i = 0; i < newWordArr.length; i++){
        if(newWordArr[i] != ' ' && newWordArr[i] != '-'){
            newWord += String.fromCharCode(newWordArr[i] + num)
        }
        else{
            newWord += newWordArr[i];
        }
    }

    
    return newWord;
}



//console.table(caesarCipher("middle-Outz", 2)); // okffng-Qwvb
//console.table(caesarCipher("Always-Look-on-the-Bright-Side-of-Life", 5));
//console.table(caesarCipher("A friend in need is a friend indeed", 20));

