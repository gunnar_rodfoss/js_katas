## Kata Ten
---
# Caesar's Cipher 
Source: Edabit 
Published by Matt in JavaScript 
algorithmscryptographystrings 
--- 
Julius Caesar protected his confidential information by encrypting it using a cipher. 
Caesar's cipher (check Resources section for more info) shifts each letter by a 
number of letters. If the shift takes you past the end of the alphabet, just rotate back 
to the front of the alphabet. In the case of a rotation by  3, w, x, y and z would map 
to z, a, b and c. 
Create a function that takes a string  s (text to be encrypted) and an integer  k (the 
rotation factor). It should return an encrypted string. 
Examples 
caesarCipher("middle-Outz", 2) ➞ "okffng-Qwvb" 
 
// m -> o 
// i -> k 
// d -> f 
// d -> f 
// l -> n 
// e -> g 
// -    - 
// O -> Q 
// u -> w 
// t -> v 
// z -> b 
 
caesarCipher("Always-Look-on-the-Bright-Side-of-Life", 5) 
➞ "Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj" 
 
caesarCipher("A friend in need is a friend indeed", 20) 
➞ "U zlcyhx ch hyyx cm u zlcyhx chxyyx" 
Notes 
All test input will be a valid ASCII string. 
 
  
Resources 
ASCII Table 
en.wikipedia.org 
For reference.  
 
String.fromCharCode() 
developer.mozilla.org 
Returns a string created from the specified sequence of UTF-16 code units. 
 
String.prototype.charCodeAt() 
developer.mozilla.org 
Returns an integer between 0 and 65535 representing the UTF-16 code unit at the 
given index. 
 
Caesar Cipher 
www.dcode.fr 
Tool to decrypt/encrypt with Caesar. Caesar cipher (or Caesar code) is a shift cipher, 
one of the most easy and most famous encryption systems. It uses  the substitution 
... 
 
Remainder operator / Modulus  (%) 
 
https://developer.mozilla.org/en-
US/docs/Web/JavaScript/Reference/Operators/Remainder 
 
http://www.java2s.com/Tutorial/JavaScript/0040__Operators/Modulus.htm 
 
 