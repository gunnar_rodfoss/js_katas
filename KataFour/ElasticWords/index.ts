 export function elasticWord(word){
    let leftS =""
    let center =""
    let rightS = ""
    if(word.length < 3){
        return word;
    }
    if(word.length % 2 == 0){
        //left SIDE
        leftS = word.slice(0,word.length/2);
        leftS += leftS[leftS.length-1] ;
        //RIGHT SIDE
        rightS = word.slice(word.length/2,word.length);
        rightS = rightS[0] + rightS;
        return leftS+rightS;
    }
    else{
        let num = (word.length - 1) / 2;
        //left SIDE
        leftS = word.slice(0,num);
        leftS += leftS[leftS.length-1] ;

        //CENTER
        center = word.slice( num, num+1);
        center = center + center + center;
        
        //RIGHT SIDE
        rightS = word.slice(num+1,word.length);
        rightS = rightS[0] + rightS;

        return leftS  + center + rightS;
    }
}
/*
console.log(elasticWord("x") );
console.log(elasticWord("ANNA") );
console.log(elasticWord("KAYAK") );
*/