
const grid = Array.from(document.querySelectorAll('.tile'))


let turnType = 0


grid.map(tile => {
    tile.addEventListener('click', (e) => {
        if (tile.innerHTML == "") {
            turnType = 1 ^ turnType;
        }

        if (turnType && tile.innerHTML == "") {

            tile.innerHTML = "×"
        }
        if (!turnType && tile.innerHTML == "") {
            tile.innerHTML = "o"
        }
    })
})
function clear() {
    grid.map(tile => {
        tile.innerHTML = ""
    })
}
function checkTiles() {
    for (let i = 0; i < grid.length - 3; i++) {
        //from left to right
        if (grid[i].innerHTML === "o" &&
            grid[i + 1].innerHTML === "o" &&
            grid[i + 2].innerHTML === "o") {
            console.log("winner");
            alert("Winner is o")
            clear()
        }
        if (grid[i].innerHTML === "×" &&
            grid[i + 1].innerHTML === "×" &&
            grid[i + 2].innerHTML === "×") {
            console.log("winner");
            alert("Winner is ×")
            clear()
        }
        //down 1
        if (grid[0].innerHTML === "o" &&
            grid[3].innerHTML === "o" &&
            grid[6].innerHTML === "o") {
            console.log("winner");
            alert("Winner is o")
            clear()
        }
        if (grid[0].innerHTML === "×" &&
            grid[3].innerHTML === "×" &&
            grid[6].innerHTML === "×") {
            console.log("winner");
            alert("Winner is ×")
            clear()
        }
        //down 2
        if (grid[1].innerHTML === "o" &&
            grid[4].innerHTML === "o" &&
            grid[7].innerHTML === "o") {
            console.log("winner");
            alert("Winner is o")
            clear()
        }
        if (grid[1].innerHTML === "×" &&
            grid[4].innerHTML === "×" &&
            grid[7].innerHTML === "×") {
            console.log("winner");
            alert("Winner is ×")
            clear()
        }
        //down 3
        if (grid[2].innerHTML === "o" &&
            grid[5].innerHTML === "o" &&
            grid[8].innerHTML === "o") {
            console.log("winner");
            alert("Winner is o")
            clear()
        }
        if (grid[2].innerHTML === "×" &&
            grid[5].innerHTML === "×" &&
            grid[8].innerHTML === "×") {
            console.log("winner");
            alert("Winner is ×")
            clear()
        }
        // X
        if (grid[0].innerHTML === "o" &&
            grid[4].innerHTML === "o" &&
            grid[8].innerHTML === "o") {
            console.log("winner");
            alert("Winner is o")
            clear()
        }
        if (grid[0].innerHTML === "×" &&
            grid[4].innerHTML === "×" &&
            grid[8].innerHTML === "×") {
            console.log("winner");
            alert("Winner is ×")
            clear()
        }
        // X
        if (grid[2].innerHTML === "o" &&
            grid[4].innerHTML === "o" &&
            grid[6].innerHTML === "o") {
            console.log("winner");
            alert("Winner is o")
            clear()
        }
        if (grid[2].innerHTML === "×" &&
            grid[4].innerHTML === "×" &&
            grid[6].innerHTML === "×") {
            console.log("winner");
            alert("Winner is ×")
            clear()
        }
    }
}
function draw() {
    //console.log("test");
    checkTiles()
    requestAnimationFrame(draw)
}
requestAnimationFrame(draw)