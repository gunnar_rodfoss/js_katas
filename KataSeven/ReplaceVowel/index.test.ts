import {replaceVowel} from	'./index';

it("shuld be k1r1ch3 for karAchi.", function(){
    const word = "karAchi";
    const newWord = "k1r1ch3";

    expect(replaceVowel(word)).toEqual(newWord);
})
it("shuld be ch2mb5r for chEmBur.", function(){
    const word = "chEmBur";
    const newWord = "ch2mb5r";

    expect(replaceVowel(word)).toEqual(newWord);
})
it("shuld be kh1ndb1r3 for khandbari.", function(){
    const word = "khandbari";
    const newWord = "kh1ndb1r3";

    expect(replaceVowel(word)).toEqual(newWord);
})

it("shuld be l2x3c1l for LexiCAl.", function(){
    const word = "LexiCAl";
    const newWord = "l2x3c1l";

    expect(replaceVowel(word)).toEqual(newWord);
})
it("shuld be f5nct34ns for fuNctionS.", function(){
    const word = "fuNctionS";
    const newWord = "f5nct34ns";

    expect(replaceVowel(word)).toEqual(newWord);
})

it("shuld be 21sy for EASY.", function(){
    const word = "EASY";
    const newWord = "21sy";

    expect(replaceVowel(word)).toEqual(newWord);
})