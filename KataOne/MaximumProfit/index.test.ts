import {maxProfit} from	'./index';

it("shuld score 14 for stocks", function(){
    const stock = [8,5,12,9,19,1];
    const max = 14;

    expect(maxProfit(stock)).toEqual(max);
})
it("shuld score 7 for stocks", function(){
    const stock = [2,4,9,3,8];
    const max = 7;

    expect(maxProfit(stock)).toEqual(max);
})
it("shuld score 0 for stocks", function(){
    const stock = [21,12,11,9,6,3];
    const max = 0;

    expect(maxProfit(stock)).toEqual(max);
})
/*// Comparing Strings
test('hello world should equal hello world', () => {
	expect("hello world").toBe("hello world");
});

// Compare numbers
test('10 + 5 should equal 15', () => {
	expect(10 + 5).toBe(15);
})

// Compare numbers to not equal
test('10 + 4 should not equal 15', () => {
	expect(10 + 4).not.toBe(15);
})

// Compare arrays
test("[1, 2, 3] should equal [1,2,3]", () => {
	expect([1, 2, 3]).toEqual([1, 2, 3])
})

// Compare arrays to not equal
test("[1, 2, 3] should not equal [1,2,3,4]", () => {
	expect([1, 2, 3]).not.toEqual([1, 2, 3,4])
})

// Compare objects
test("{ message: 'hello' } should equal { message: 'hello' }", () => {
	expect([1, 2, 3]).toEqual([1, 2, 3])
})

// Compare objects to not equal
test("{ message: 'hello' } should not equal { message: 'goodbye' }", () => {
	expect({ message: 'hello' }).not.toEqual({ message: 'goodbye' })
})


*/