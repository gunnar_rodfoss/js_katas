export function maxProfit(arr){
    let num = 0;
    let maxProf = 0;
    for(let i = 0; i < arr.length-1; i++){
        for(let j = i+1; j < arr.length-1; j++){
            let temp = arr[j] - arr[i];
            if(temp > 0 && temp > maxProf){
                maxProf = temp;
            }
        }

    }
    console.log(maxProf);
    return maxProf;
}

//maxProfit([8,5,12,9,19,1]);
//maxProfit([2,4,9,3,8]);
//maxProfit([21,12,11,9,6,3]);