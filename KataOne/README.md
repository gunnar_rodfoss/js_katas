
## Maximum Profit  

---

Given a list of daily stock prices (array of integers), return the maximum 
profit  for the buy and sell prices.  
 
You need to maximize the single buy/sell profit. If you can't make any profit, 
simply return 0. 
 
i.e., Given the array [8, 5, 12, 9, 19, 1], buying at 5 and selling at 
19 will yield 14 profit.  

---

# Examples 

---
 
maximumProfit([8, 5, 12, 9, 19, 1]) -> 14 
 
maximumProfit([2, 4, 9, 3, 8]) -> 7 
 
maximumProfit([21, 12, 11, 9, 6, 3]) -> 0 

---

# Notes: 
 
• You can always expect an array of integers. 
• You can always expect positive integers 
• Return 0 if no profit can be made. 
 